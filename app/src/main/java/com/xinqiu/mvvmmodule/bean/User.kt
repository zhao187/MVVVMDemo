package com.xinqiu.mvvmmodule.bean

import android.databinding.ObservableField
import android.databinding.ObservableInt

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/11/14
 * desc:用户信息
 */
class User
{
    var firstName = ObservableField<String>()
    var lastName=ObservableField<String>()
    var age= ObservableInt()
}