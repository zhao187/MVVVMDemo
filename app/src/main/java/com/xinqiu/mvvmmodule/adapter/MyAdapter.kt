package com.xinqiu.mvvmmodule.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.xinqiu.mvvmmodule.bean.User
import com.xinqiu.mvvmmodule.databinding.UserItemBinding
import android.view.LayoutInflater



/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/11/14
 * desc:适配器
 */
class MyAdapter(val mData:ArrayList<User>?) : RecyclerView.Adapter<MyAdapter.MyHolder>(){

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bindTo(mData!![position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int):MyHolder {
        return MyHolder.create(LayoutInflater.from(parent.context), parent)
    }

    override fun getItemCount(): Int = mData?.size ?: 0




    class MyHolder private constructor(private val mBinding: UserItemBinding) : RecyclerView.ViewHolder(mBinding.root) {

        fun bindTo(user: User) {
            mBinding.user2 = user
            mBinding.executePendingBindings()
        }

        companion object {
            fun create(inflater: LayoutInflater, parent: ViewGroup): MyHolder {
                val binding = UserItemBinding.inflate(inflater, parent, false)
                return MyHolder(binding)
            }
        }

    }
}