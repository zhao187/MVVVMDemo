package com.xinqiu.mvvmmodule

import android.content.Intent
import android.databinding.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.xinqiu.mvvmmodule.bean.User
import com.xinqiu.mvvmmodule.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    val mName = "MM"

    lateinit var user:User

    lateinit var user1:ObservableMap<String, Any>

    lateinit var user2: ObservableArrayList<Any>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        user=User()
        user.firstName.set("zhang")
        user.lastName.set("lisi")

        binding?.user=user

        binding?.imageUrl="https://upload-images.jianshu.io/upload_images/5330554-539237268849415d.png"

        binding?.mainActivity=this

        user1= ObservableArrayMap<String, Any>()
        user1["ni1"] = "Google"
        user1["ni2"]="ni hao"
        user1["age"]=17

        binding?.user1=user1

        user2= ObservableArrayList()

        user2.add("ma yun")
        user2.add("ni hao")
        user2.add(18)

        binding?.user2=user2



        delay()
    }

    //延时更新数据
    private fun delay() {
        Handler().postDelayed(Runnable {
            kotlin.run {
                user.firstName.set("wangwu")
            }
        },2000)
    }

    fun onClick(view:View)
    {
        Toast.makeText(this,"点击事件", Toast.LENGTH_LONG).show()

        startActivity(Intent(this,ListActivity::class.java))
    }
}
