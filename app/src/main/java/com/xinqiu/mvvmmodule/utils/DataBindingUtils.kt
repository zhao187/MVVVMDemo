package com.xinqiu.mvvmmodule.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * @author Steven.zhao
 * email:hongtu.zhao@goodwinsoft.net
 * date:2018/11/14
 * desc:图片
 */
object DataBindingUtils {

    @BindingAdapter("bind:image")
    @JvmStatic
    fun loadImage(view:ImageView?,url:String?)
    {
        Glide.with(view?.context!!).load(url).into(view)
    }
}