package com.xinqiu.mvvmmodule

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.xinqiu.mvvmmodule.bean.User
import com.xinqiu.mvvmmodule.adapter.MyAdapter
import android.support.v7.widget.LinearLayoutManager
import com.xinqiu.mvvmmodule.databinding.ActivityListBinding


class ListActivity : AppCompatActivity() {

    private lateinit var binding:ActivityListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_list)

        val data = ArrayList<User>()
        for (i in 0..19) {
            val user2 = User()
            user2.age.set(30)
            user2.firstName.set("Micheal $i")
            user2.lastName.set("Jack $i")
            data.add(user2)
        }

        val layoutManager = LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = MyAdapter(data)
    }
}
